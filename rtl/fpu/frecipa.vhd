----------------------------------------------------------------------------------------------------
-- Copyright (c) 2024 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- This is a fast reciprocal approximation implementation for floating-point values.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types.all;

entity frecipa is
  generic(
    WIDTH : positive := 32;
    EXP_BITS : positive := 8;
    EXP_BIAS : positive := 127;
    FRACT_BITS : positive := 23
  );
  port(
    i_props : in T_FLOAT_PROPS;
    i_exponent : in std_logic_vector(EXP_BITS-1 downto 0);
    i_significand : in std_logic_vector(FRACT_BITS downto 0);

    -- Outputs (async).
    o_result : out std_logic_vector(WIDTH-1 downto 0)
  );
end frecipa;

architecture rtl of frecipa is
  -- Helpers for creating constants.
  function get_lut_bits(word_size : integer) return integer is
  begin
    if word_size >= 16 then
      return 8;
    else
      return 3;
    end if;
  end function;

  function get_approx_bits(word_size : integer) return integer is
  begin
    if word_size >= 16 then
      return 9;
    else
      return 3;
    end if;
  end function;

  -- Constants.
  constant LUT_BITS : positive := get_lut_bits(WIDTH);
  constant APPROX_BITS : positive := get_approx_bits(WIDTH);
  constant MAX_EXPONENT : std_logic_vector(EXP_BITS-1 downto 0) := ones(EXP_BITS-1) & '0';

  signal s_lut_idx : std_logic_vector(LUT_BITS-1 downto 0);
  signal s_approx : std_logic_vector(APPROX_BITS-1 downto 0);
  signal s_significand : std_logic_vector(FRACT_BITS downto 0);
  signal s_underflow : std_logic;
  signal s_exponent : std_logic_vector(EXP_BITS-1 downto 0);
  signal s_props : T_FLOAT_PROPS;

  function extend_fraction(approx : std_logic_vector(APPROX_BITS-1 downto 0)) return std_logic_vector is
    variable v_significand : std_logic_vector(FRACT_BITS downto 0);
    variable v_pos : integer;
    variable v_bits : integer;
  begin
    v_significand(FRACT_BITS) := '1';
    v_pos := FRACT_BITS - 1;
    while v_pos >= 0 loop
      if v_pos >= APPROX_BITS - 1 then
        v_bits := APPROX_BITS;
      else
        v_bits := v_pos + 1;
      end if;
      v_significand(v_pos downto v_pos - v_bits + 1) := approx(APPROX_BITS - 1 downto APPROX_BITS - v_bits);
      v_pos := v_pos - APPROX_BITS;
    end loop;
    return v_significand;
  end function;
begin
  --================================================================================================
  -- Generate the fraction part using an approximation LUT.
  --================================================================================================

  -- Use the most significant bits from the fraction as the LUT index.
  s_lut_idx <= i_significand(FRACT_BITS-1 downto FRACT_BITS-LUT_BITS);

  -- Get the approximation from the LUT.
  LUT_GEN : if LUT_BITS = 8 generate
  begin
    -- This LUT is used for 32-bit and 16-bit floating-point.
  ApproximationLUT: with s_lut_idx select
    s_approx <=
      "111111110" when "00000000",
      "111111010" when "00000001",
      "111110110" when "00000010",
      "111110010" when "00000011",
      "111101110" when "00000100",
      "111101010" when "00000101",
      "111100111" when "00000110",
      "111100011" when "00000111",
      "111011111" when "00001000",
      "111011011" when "00001001",
      "111011000" when "00001010",
      "111010100" when "00001011",
      "111010000" when "00001100",
      "111001101" when "00001101",
      "111001001" when "00001110",
      "111000110" when "00001111",
      "111000010" when "00010000",
      "110111110" when "00010001",
      "110111011" when "00010010",
      "110111000" when "00010011",
      "110110100" when "00010100",
      "110110001" when "00010101",
      "110101101" when "00010110",
      "110101010" when "00010111",
      "110100111" when "00011000",
      "110100011" when "00011001",
      "110100000" when "00011010",
      "110011101" when "00011011",
      "110011001" when "00011100",
      "110010110" when "00011101",
      "110010011" when "00011110",
      "110010000" when "00011111",
      "110001101" when "00100000",
      "110001010" when "00100001",
      "110000110" when "00100010",
      "110000011" when "00100011",
      "110000000" when "00100100",
      "101111101" when "00100101",
      "101111010" when "00100110",
      "101110111" when "00100111",
      "101110100" when "00101000",
      "101110001" when "00101001",
      "101101110" when "00101010",
      "101101011" when "00101011",
      "101101000" when "00101100",
      "101100101" when "00101101",
      "101100011" when "00101110",
      "101100000" when "00101111",
      "101011101" when "00110000",
      "101011010" when "00110001",
      "101010111" when "00110010",
      "101010101" when "00110011",
      "101010010" when "00110100",
      "101001111" when "00110101",
      "101001100" when "00110110",
      "101001010" when "00110111",
      "101000111" when "00111000",
      "101000100" when "00111001",
      "101000010" when "00111010",
      "100111111" when "00111011",
      "100111100" when "00111100",
      "100111010" when "00111101",
      "100110111" when "00111110",
      "100110100" when "00111111",
      "100110010" when "01000000",
      "100101111" when "01000001",
      "100101101" when "01000010",
      "100101010" when "01000011",
      "100101000" when "01000100",
      "100100101" when "01000101",
      "100100011" when "01000110",
      "100100000" when "01000111",
      "100011110" when "01001000",
      "100011100" when "01001001",
      "100011001" when "01001010",
      "100010111" when "01001011",
      "100010100" when "01001100",
      "100010010" when "01001101",
      "100010000" when "01001110",
      "100001101" when "01001111",
      "100001011" when "01010000",
      "100001001" when "01010001",
      "100000110" when "01010010",
      "100000100" when "01010011",
      "100000010" when "01010100",
      "100000000" when "01010101",
      "011111101" when "01010110",
      "011111011" when "01010111",
      "011111001" when "01011000",
      "011110111" when "01011001",
      "011110101" when "01011010",
      "011110010" when "01011011",
      "011110000" when "01011100",
      "011101110" when "01011101",
      "011101100" when "01011110",
      "011101010" when "01011111",
      "011101000" when "01100000",
      "011100110" when "01100001",
      "011100011" when "01100010",
      "011100001" when "01100011",
      "011011111" when "01100100",
      "011011101" when "01100101",
      "011011011" when "01100110",
      "011011001" when "01100111",
      "011010111" when "01101000",
      "011010101" when "01101001",
      "011010011" when "01101010",
      "011010001" when "01101011",
      "011001111" when "01101100",
      "011001101" when "01101101",
      "011001011" when "01101110",
      "011001001" when "01101111",
      "011000111" when "01110000",
      "011000101" when "01110001",
      "011000100" when "01110010",
      "011000010" when "01110011",
      "011000000" when "01110100",
      "010111110" when "01110101",
      "010111100" when "01110110",
      "010111010" when "01110111",
      "010111000" when "01111000",
      "010110110" when "01111001",
      "010110101" when "01111010",
      "010110011" when "01111011",
      "010110001" when "01111100",
      "010101111" when "01111101",
      "010101101" when "01111110",
      "010101100" when "01111111",
      "010101010" when "10000000",
      "010101000" when "10000001",
      "010100110" when "10000010",
      "010100101" when "10000011",
      "010100011" when "10000100",
      "010100001" when "10000101",
      "010011111" when "10000110",
      "010011110" when "10000111",
      "010011100" when "10001000",
      "010011010" when "10001001",
      "010011000" when "10001010",
      "010010111" when "10001011",
      "010010101" when "10001100",
      "010010011" when "10001101",
      "010010010" when "10001110",
      "010010000" when "10001111",
      "010001111" when "10010000",
      "010001101" when "10010001",
      "010001011" when "10010010",
      "010001010" when "10010011",
      "010001000" when "10010100",
      "010000110" when "10010101",
      "010000101" when "10010110",
      "010000011" when "10010111",
      "010000010" when "10011000",
      "010000000" when "10011001",
      "001111111" when "10011010",
      "001111101" when "10011011",
      "001111100" when "10011100",
      "001111010" when "10011101",
      "001111000" when "10011110",
      "001110111" when "10011111",
      "001110101" when "10100000",
      "001110100" when "10100001",
      "001110010" when "10100010",
      "001110001" when "10100011",
      "001101111" when "10100100",
      "001101110" when "10100101",
      "001101100" when "10100110",
      "001101011" when "10100111",
      "001101010" when "10101000",
      "001101000" when "10101001",
      "001100111" when "10101010",
      "001100101" when "10101011",
      "001100100" when "10101100",
      "001100010" when "10101101",
      "001100001" when "10101110",
      "001100000" when "10101111",
      "001011110" when "10110000",
      "001011101" when "10110001",
      "001011011" when "10110010",
      "001011010" when "10110011",
      "001011001" when "10110100",
      "001010111" when "10110101",
      "001010110" when "10110110",
      "001010100" when "10110111",
      "001010011" when "10111000",
      "001010010" when "10111001",
      "001010000" when "10111010",
      "001001111" when "10111011",
      "001001110" when "10111100",
      "001001100" when "10111101",
      "001001011" when "10111110",
      "001001010" when "10111111",
      "001001000" when "11000000",
      "001000111" when "11000001",
      "001000110" when "11000010",
      "001000101" when "11000011",
      "001000011" when "11000100",
      "001000010" when "11000101",
      "001000001" when "11000110",
      "001000000" when "11000111",
      "000111110" when "11001000",
      "000111101" when "11001001",
      "000111100" when "11001010",
      "000111010" when "11001011",
      "000111001" when "11001100",
      "000111000" when "11001101",
      "000110111" when "11001110",
      "000110110" when "11001111",
      "000110100" when "11010000",
      "000110011" when "11010001",
      "000110010" when "11010010",
      "000110001" when "11010011",
      "000110000" when "11010100",
      "000101110" when "11010101",
      "000101101" when "11010110",
      "000101100" when "11010111",
      "000101011" when "11011000",
      "000101010" when "11011001",
      "000101000" when "11011010",
      "000100111" when "11011011",
      "000100110" when "11011100",
      "000100101" when "11011101",
      "000100100" when "11011110",
      "000100011" when "11011111",
      "000100010" when "11100000",
      "000100000" when "11100001",
      "000011111" when "11100010",
      "000011110" when "11100011",
      "000011101" when "11100100",
      "000011100" when "11100101",
      "000011011" when "11100110",
      "000011010" when "11100111",
      "000011001" when "11101000",
      "000011000" when "11101001",
      "000010110" when "11101010",
      "000010101" when "11101011",
      "000010100" when "11101100",
      "000010011" when "11101101",
      "000010010" when "11101110",
      "000010001" when "11101111",
      "000010000" when "11110000",
      "000001111" when "11110001",
      "000001110" when "11110010",
      "000001101" when "11110011",
      "000001100" when "11110100",
      "000001011" when "11110101",
      "000001010" when "11110110",
      "000001001" when "11110111",
      "000001000" when "11111000",
      "000000111" when "11111001",
      "000000110" when "11111010",
      "000000101" when "11111011",
      "000000100" when "11111100",
      "000000011" when "11111101",
      "000000010" when "11111110",
      "000000001" when "11111111",
      (others => '-') when others;
  else generate
    -- This LUT is used for 8-bit floating-point.
    ApproximationLUT: with s_lut_idx select
      s_approx <=
        "111" when "000",
        "101" when "001",
        "100" when "010",
        "011" when "011",
        "010" when "100",
        "001" when "101",
        "001" when "110",
        "000" when "111",
        (others => '-') when others;
  end generate;

  -- Widen the approximation to the full significand width.
  s_significand <= extend_fraction(s_approx);

  --================================================================================================
  -- Generate the exponent part (negate).
  --================================================================================================

  process (ALL)
    variable v_exp : unsigned(EXP_BITS downto 0);
  begin
    v_exp := unsigned('0' & i_exponent);
    v_exp := EXP_BIAS*2 - 1 - v_exp;
    s_exponent <= std_logic_vector(v_exp(EXP_BITS-1 downto 0));
  end process;

  -- We can get underflow if the source exponent is too big.
  s_underflow <= '1' when i_exponent = MAX_EXPONENT else '0';

  --================================================================================================
  -- Generate the props.
  --================================================================================================

  s_props.is_neg <= i_props.is_neg;
  s_props.is_nan <= i_props.is_nan;
  s_props.is_inf <= i_props.is_zero;
  s_props.is_zero <= i_props.is_inf or s_underflow;

  --==================================================================================================
  -- Compose the final floating-point value.
  --==================================================================================================

  Compose: entity work.float_compose
    generic map (
      WIDTH => WIDTH,
      EXP_BITS => EXP_BITS,
      FRACT_BITS => FRACT_BITS
    )
    port map (
      i_props => s_props,
      i_exponent => s_exponent,
      i_significand => s_significand,
      o_result => o_result
    );
end rtl;
