----------------------------------------------------------------------------------------------------
-- Copyright (c) 2024 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- This is a fast reciprocal square root approximation implementation for floating-point values.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types.all;

entity frsqrta is
  generic(
    WIDTH : positive := 32;
    EXP_BITS : positive := 8;
    EXP_BIAS : positive := 127;
    FRACT_BITS : positive := 23
  );
  port(
    i_props : in T_FLOAT_PROPS;
    i_exponent : in std_logic_vector(EXP_BITS-1 downto 0);
    i_significand : in std_logic_vector(FRACT_BITS downto 0);

    -- Outputs (async).
    o_result : out std_logic_vector(WIDTH-1 downto 0)
  );
end frsqrta;

architecture rtl of frsqrta is
  -- Helpers for creating constants.
  function get_lut_bits(word_size : integer) return integer is
  begin
    if word_size >= 16 then
      return 8;
    else
      return 4;
    end if;
  end function;

  function get_approx_bits(word_size : integer) return integer is
  begin
    if word_size >= 16 then
      return 9;
    else
      return 3;
    end if;
  end function;

  -- Constants.
  constant LUT_BITS : positive := get_lut_bits(WIDTH);
  constant APPROX_BITS : positive := get_approx_bits(WIDTH);
  constant MAX_EXPONENT : std_logic_vector(EXP_BITS-1 downto 0) := ones(EXP_BITS-1) & '0';

  signal s_lut_idx : std_logic_vector(LUT_BITS-1 downto 0);
  signal s_approx : std_logic_vector(APPROX_BITS-1 downto 0);
  signal s_significand : std_logic_vector(FRACT_BITS downto 0);
  signal s_exponent : std_logic_vector(EXP_BITS-1 downto 0);
  signal s_props : T_FLOAT_PROPS;

  function extend_fraction(approx : std_logic_vector(APPROX_BITS-1 downto 0)) return std_logic_vector is
    variable v_significand : std_logic_vector(FRACT_BITS downto 0);
    variable v_pos : integer;
    variable v_bits : integer;
  begin
    v_significand(FRACT_BITS) := '1';
    v_pos := FRACT_BITS - 1;
    while v_pos >= 0 loop
      if v_pos >= APPROX_BITS - 1 then
        v_bits := APPROX_BITS;
      else
        v_bits := v_pos + 1;
      end if;
      v_significand(v_pos downto v_pos - v_bits + 1) := approx(APPROX_BITS - 1 downto APPROX_BITS - v_bits);
      v_pos := v_pos - APPROX_BITS;
    end loop;
    return v_significand;
  end function;
begin
  --================================================================================================
  -- Generate the fraction part using an approximation LUT.
  --================================================================================================

  -- Use the (inverted) least significant bit from to exponent and the most significant bits from
  -- the fraction as the LUT index.
  s_lut_idx <= (not i_exponent(0)) & i_significand(FRACT_BITS-1 downto FRACT_BITS-(LUT_BITS-1));

  -- Get the approximation from the LUT.
  LUT_GEN : if LUT_BITS = 8 generate
  begin
    -- This LUT is used for 32-bit and 16-bit floating-point.
    ApproximationLUT: with s_lut_idx select
      s_approx <=
        "111111110" when "00000000",
        "111111010" when "00000001",
        "111110110" when "00000010",
        "111110010" when "00000011",
        "111101110" when "00000100",
        "111101011" when "00000101",
        "111100111" when "00000110",
        "111100011" when "00000111",
        "111100000" when "00001000",
        "111011100" when "00001001",
        "111011000" when "00001010",
        "111010101" when "00001011",
        "111010001" when "00001100",
        "111001110" when "00001101",
        "111001011" when "00001110",
        "111000111" when "00001111",
        "111000100" when "00010000",
        "111000000" when "00010001",
        "110111101" when "00010010",
        "110111010" when "00010011",
        "110110111" when "00010100",
        "110110100" when "00010101",
        "110110000" when "00010110",
        "110101101" when "00010111",
        "110101010" when "00011000",
        "110100111" when "00011001",
        "110100100" when "00011010",
        "110100001" when "00011011",
        "110011110" when "00011100",
        "110011011" when "00011101",
        "110011000" when "00011110",
        "110010101" when "00011111",
        "110010010" when "00100000",
        "110010000" when "00100001",
        "110001101" when "00100010",
        "110001010" when "00100011",
        "110000111" when "00100100",
        "110000101" when "00100101",
        "110000010" when "00100110",
        "101111111" when "00100111",
        "101111100" when "00101000",
        "101111010" when "00101001",
        "101110111" when "00101010",
        "101110101" when "00101011",
        "101110010" when "00101100",
        "101110000" when "00101101",
        "101101101" when "00101110",
        "101101011" when "00101111",
        "101101000" when "00110000",
        "101100110" when "00110001",
        "101100011" when "00110010",
        "101100001" when "00110011",
        "101011110" when "00110100",
        "101011100" when "00110101",
        "101011010" when "00110110",
        "101010111" when "00110111",
        "101010101" when "00111000",
        "101010011" when "00111001",
        "101010000" when "00111010",
        "101001110" when "00111011",
        "101001100" when "00111100",
        "101001010" when "00111101",
        "101000111" when "00111110",
        "101000101" when "00111111",
        "101000011" when "01000000",
        "101000001" when "01000001",
        "100111111" when "01000010",
        "100111101" when "01000011",
        "100111010" when "01000100",
        "100111000" when "01000101",
        "100110110" when "01000110",
        "100110100" when "01000111",
        "100110010" when "01001000",
        "100110000" when "01001001",
        "100101110" when "01001010",
        "100101100" when "01001011",
        "100101010" when "01001100",
        "100101000" when "01001101",
        "100100110" when "01001110",
        "100100100" when "01001111",
        "100100010" when "01010000",
        "100100000" when "01010001",
        "100011111" when "01010010",
        "100011101" when "01010011",
        "100011011" when "01010100",
        "100011001" when "01010101",
        "100010111" when "01010110",
        "100010101" when "01010111",
        "100010011" when "01011000",
        "100010010" when "01011001",
        "100010000" when "01011010",
        "100001110" when "01011011",
        "100001100" when "01011100",
        "100001010" when "01011101",
        "100001001" when "01011110",
        "100000111" when "01011111",
        "100000101" when "01100000",
        "100000011" when "01100001",
        "100000010" when "01100010",
        "100000000" when "01100011",
        "011111110" when "01100100",
        "011111101" when "01100101",
        "011111011" when "01100110",
        "011111001" when "01100111",
        "011111000" when "01101000",
        "011110110" when "01101001",
        "011110101" when "01101010",
        "011110011" when "01101011",
        "011110001" when "01101100",
        "011110000" when "01101101",
        "011101110" when "01101110",
        "011101101" when "01101111",
        "011101011" when "01110000",
        "011101001" when "01110001",
        "011101000" when "01110010",
        "011100110" when "01110011",
        "011100101" when "01110100",
        "011100011" when "01110101",
        "011100010" when "01110110",
        "011100000" when "01110111",
        "011011111" when "01111000",
        "011011101" when "01111001",
        "011011100" when "01111010",
        "011011011" when "01111011",
        "011011001" when "01111100",
        "011011000" when "01111101",
        "011010110" when "01111110",
        "011010101" when "01111111",
        "011010011" when "10000000",
        "011010000" when "10000001",
        "011001101" when "10000010",
        "011001010" when "10000011",
        "011001000" when "10000100",
        "011000101" when "10000101",
        "011000010" when "10000110",
        "011000000" when "10000111",
        "010111101" when "10001000",
        "010111011" when "10001001",
        "010111000" when "10001010",
        "010110110" when "10001011",
        "010110011" when "10001100",
        "010110001" when "10001101",
        "010101110" when "10001110",
        "010101100" when "10001111",
        "010101001" when "10010000",
        "010100111" when "10010001",
        "010100101" when "10010010",
        "010100011" when "10010011",
        "010100000" when "10010100",
        "010011110" when "10010101",
        "010011100" when "10010110",
        "010011010" when "10010111",
        "010010111" when "10011000",
        "010010101" when "10011001",
        "010010011" when "10011010",
        "010010001" when "10011011",
        "010001111" when "10011100",
        "010001101" when "10011101",
        "010001011" when "10011110",
        "010001001" when "10011111",
        "010000111" when "10100000",
        "010000101" when "10100001",
        "010000011" when "10100010",
        "010000001" when "10100011",
        "001111111" when "10100100",
        "001111101" when "10100101",
        "001111011" when "10100110",
        "001111001" when "10100111",
        "001110111" when "10101000",
        "001110101" when "10101001",
        "001110011" when "10101010",
        "001110010" when "10101011",
        "001110000" when "10101100",
        "001101110" when "10101101",
        "001101100" when "10101110",
        "001101010" when "10101111",
        "001101001" when "10110000",
        "001100111" when "10110001",
        "001100101" when "10110010",
        "001100011" when "10110011",
        "001100010" when "10110100",
        "001100000" when "10110101",
        "001011110" when "10110110",
        "001011101" when "10110111",
        "001011011" when "10111000",
        "001011001" when "10111001",
        "001011000" when "10111010",
        "001010110" when "10111011",
        "001010101" when "10111100",
        "001010011" when "10111101",
        "001010010" when "10111110",
        "001010000" when "10111111",
        "001001110" when "11000000",
        "001001101" when "11000001",
        "001001011" when "11000010",
        "001001010" when "11000011",
        "001001000" when "11000100",
        "001000111" when "11000101",
        "001000101" when "11000110",
        "001000100" when "11000111",
        "001000011" when "11001000",
        "001000001" when "11001001",
        "001000000" when "11001010",
        "000111110" when "11001011",
        "000111101" when "11001100",
        "000111011" when "11001101",
        "000111010" when "11001110",
        "000111001" when "11001111",
        "000110111" when "11010000",
        "000110110" when "11010001",
        "000110101" when "11010010",
        "000110011" when "11010011",
        "000110010" when "11010100",
        "000110001" when "11010101",
        "000101111" when "11010110",
        "000101110" when "11010111",
        "000101101" when "11011000",
        "000101011" when "11011001",
        "000101010" when "11011010",
        "000101001" when "11011011",
        "000101000" when "11011100",
        "000100110" when "11011101",
        "000100101" when "11011110",
        "000100100" when "11011111",
        "000100011" when "11100000",
        "000100010" when "11100001",
        "000100000" when "11100010",
        "000011111" when "11100011",
        "000011110" when "11100100",
        "000011101" when "11100101",
        "000011100" when "11100110",
        "000011010" when "11100111",
        "000011001" when "11101000",
        "000011000" when "11101001",
        "000010111" when "11101010",
        "000010110" when "11101011",
        "000010101" when "11101100",
        "000010100" when "11101101",
        "000010010" when "11101110",
        "000010001" when "11101111",
        "000010000" when "11110000",
        "000001111" when "11110001",
        "000001110" when "11110010",
        "000001101" when "11110011",
        "000001100" when "11110100",
        "000001011" when "11110101",
        "000001010" when "11110110",
        "000001001" when "11110111",
        "000001000" when "11111000",
        "000000111" when "11111001",
        "000000110" when "11111010",
        "000000101" when "11111011",
        "000000100" when "11111100",
        "000000011" when "11111101",
        "000000010" when "11111110",
        "000000001" when "11111111",
        (others => '-') when others;
  else generate
    -- This LUT is used for 8-bit floating-point.
    ApproximationLUT: with s_lut_idx select
      s_approx <=
        "111" when "0000",
        "111" when "0001",
        "110" when "0010",
        "101" when "0011",
        "101" when "0100",
        "100" when "0101",
        "100" when "0110",
        "011" when "0111",
        "011" when "1000",
        "010" when "1001",
        "010" when "1010",
        "001" when "1011",
        "001" when "1100",
        "001" when "1101",
        "000" when "1110",
        "000" when "1111",
        (others => '-') when others;
  end generate;

  -- Widen the approximation to the full significand width.
  s_significand <= extend_fraction(s_approx);

  --================================================================================================
  -- Generate the exponent part (halve and negate).
  --================================================================================================

  process (ALL)
    variable v_exp : signed(EXP_BITS downto 0);
  begin
    v_exp := signed('0' & i_exponent) - EXP_BIAS;
    v_exp := v_exp(EXP_BITS) & v_exp(EXP_BITS downto 1);  -- >> 1
    v_exp := EXP_BIAS - 1 - v_exp;
    s_exponent <= std_logic_vector(v_exp(EXP_BITS-1 downto 0));
  end process;

  --================================================================================================
  -- Generate the props.
  --================================================================================================

  s_props.is_neg <= i_props.is_neg;
  s_props.is_nan <= i_props.is_nan or i_props.is_neg;
  s_props.is_inf <= i_props.is_zero;
  s_props.is_zero <= i_props.is_inf;

  --==================================================================================================
  -- Compose the final floating-point value.
  --==================================================================================================

  Compose: entity work.float_compose
    generic map (
      WIDTH => WIDTH,
      EXP_BITS => EXP_BITS,
      FRACT_BITS => FRACT_BITS
    )
    port map (
      i_props => s_props,
      i_exponent => s_exponent,
      i_significand => s_significand,
      o_result => o_result
    );
end rtl;
