----------------------------------------------------------------------------------------------------
-- Copyright (c) 2020 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- The instruction cache is a simple direct-mapped read-only cache, with configurable size and
-- cache line size.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config.all;

entity icache is
  generic(
    G_LOG2_LINES : integer := 7;
    G_LOG2_SUBBLOCKS_PER_LINE : integer := 3;
    G_REQ_FIFO_DEPTH :integer := 16
  );
  port(
    -- Control signals.
    i_clk : in std_logic;
    i_rst : in std_logic;
    i_invalidate : in std_logic;

    -- Instruction interface (slave).
    i_instr_req : in std_logic;
    i_instr_adr : in std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);
    o_instr_dat : out std_logic_vector(C_MEM_WIDTH-1 downto 0);
    o_instr_ack : out std_logic;

    -- Memory interface (WB master).
    o_mem_cyc : out std_logic;
    o_mem_stb : out std_logic;
    o_mem_adr : out std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);
    i_mem_dat : in std_logic_vector(C_MEM_WIDTH-1 downto 0);
    i_mem_ack : in std_logic;
    i_mem_stall : in std_logic;
    i_mem_err : in std_logic
  );
end icache;

architecture rtl of icache is
  --------------------------------------------------------------------------------------------------
  -- Cache memory configuration & signals.
  --------------------------------------------------------------------------------------------------

  constant C_LINES : integer := 2**G_LOG2_LINES;
  constant C_SUBBLOCKS_PER_LINE : integer := 2**G_LOG2_SUBBLOCKS_PER_LINE;

  constant C_TAG_WIDTH : integer := 1 + (C_WORD_SIZE-C_MEM_ADR_LSB - G_LOG2_LINES - G_LOG2_SUBBLOCKS_PER_LINE);
  constant C_TAG_ADDR_BITS : integer := G_LOG2_LINES;
  constant C_MAX_TAG_ADDR : unsigned := to_unsigned((2 ** C_TAG_ADDR_BITS) - 1, C_TAG_ADDR_BITS);
  constant C_DATA_ADDR_BITS : integer := G_LOG2_LINES + G_LOG2_SUBBLOCKS_PER_LINE;

  signal s_tagmem_wr_data : std_logic_vector(C_TAG_WIDTH-1 downto 0);
  signal s_tagmem_wr_addr : std_logic_vector(C_TAG_ADDR_BITS-1 downto 0);
  signal s_tagmem_wr_en : std_logic;
  signal s_tagmem_rd_addr : std_logic_vector(C_TAG_ADDR_BITS-1 downto 0);
  signal s_tagmem_rd_data : std_logic_vector(C_TAG_WIDTH-1 downto 0);
  signal s_forwarded_tagmem_data : std_logic_vector(C_TAG_WIDTH-1 downto 0);
  signal s_use_forwarded_tagmem_data : std_logic;
  signal s_tagmem_rd_data_actual : std_logic_vector(C_TAG_WIDTH-1 downto 0);

  signal s_datamem_wr_data : std_logic_vector(C_MEM_WIDTH-1 downto 0);
  signal s_datamem_wr_addr : std_logic_vector(C_DATA_ADDR_BITS-1 downto 0);
  signal s_datamem_wr_en : std_logic;
  signal s_datamem_rd_addr : std_logic_vector(C_DATA_ADDR_BITS-1 downto 0);
  signal s_datamem_rd_data : std_logic_vector(C_MEM_WIDTH-1 downto 0);
  signal s_forwarded_datamem_data : std_logic_vector(C_MEM_WIDTH-1 downto 0);
  signal s_use_forwarded_datamem_data : std_logic;
  signal s_datamem_rd_data_actual : std_logic_vector(C_MEM_WIDTH-1 downto 0);

  --------------------------------------------------------------------------------------------------
  -- Cache lookup signals.
  --------------------------------------------------------------------------------------------------

  signal s_ic1_instr_req : std_logic;
  signal s_ic1_instr_adr : std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);
  signal s_requested_subblock_idx : std_logic_vector(G_LOG2_SUBBLOCKS_PER_LINE-1 downto 0);

  --------------------------------------------------------------------------------------------------
  -- Cache state machine.
  --------------------------------------------------------------------------------------------------

  -- State machine states.
  type T_IC_STATE is (
    RESET,
    INVALIDATE,
    READY,
    UPDATE_CACHE_LINE
  );

  signal s_state : T_IC_STATE;
  signal s_next_state : T_IC_STATE;

  signal s_invalidate_requested : std_logic;
  signal s_invalidate_addr : unsigned(C_TAG_ADDR_BITS-1 downto 0);
  signal s_next_invalidate_addr : unsigned(C_TAG_ADDR_BITS-1 downto 0);

  signal s_subblock_idx : std_logic_vector(G_LOG2_SUBBLOCKS_PER_LINE-1 downto 0);
  signal s_next_subblock_idx : std_logic_vector(G_LOG2_SUBBLOCKS_PER_LINE-1 downto 0);
  signal s_last_subblock_idx : std_logic_vector(G_LOG2_SUBBLOCKS_PER_LINE-1 downto 0);

  signal s_cache_line_adr : std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);
  signal s_next_cache_line_adr : std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);
  signal s_cache_line_done : std_logic;
  signal s_next_cache_line_done : std_logic;

  signal s_repeat_request : std_logic;
  signal s_next_repeat_request : std_logic;

  --------------------------------------------------------------------------------------------------
  -- Pending requests FIFO configuration & signals.
  --------------------------------------------------------------------------------------------------

  constant C_REQ_FIFO_WIDTH : integer := G_LOG2_SUBBLOCKS_PER_LINE;

  signal s_fifo_wr_en : std_logic;
  signal s_fifo_wr_data : std_logic_vector(C_REQ_FIFO_WIDTH-1 downto 0);
  signal s_fifo_full : std_logic;
  signal s_fifo_rd_en : std_logic;
  signal s_fifo_rd_data : std_logic_vector(C_REQ_FIFO_WIDTH-1 downto 0);
  signal s_fifo_empty : std_logic;

  --------------------------------------------------------------------------------------------------
  -- Memory request signals.
  --------------------------------------------------------------------------------------------------

  signal s_can_start_req : std_logic;
  signal s_start_req : std_logic;

  signal s_req_en : std_logic;
  signal s_req_adr : std_logic_vector(C_WORD_SIZE-1 downto C_MEM_ADR_LSB);

  signal s_resp_ack : std_logic;
  signal s_resp_dat : std_logic_vector(C_MEM_WIDTH-1 downto 0);
  signal s_resp_subblock_idx : std_logic_vector(G_LOG2_SUBBLOCKS_PER_LINE-1 downto 0);

  --------------------------------------------------------------------------------------------------
  -- Helper functions.
  --------------------------------------------------------------------------------------------------

  function make_valid_tag(addr : std_logic_vector) return std_logic_vector is
  begin
    return "1" & addr(addr'left downto addr'left - (C_TAG_WIDTH-1) + 1);
  end function;

  function make_invalid_tag return std_logic_vector is
  begin
    return "0" & std_logic_vector(to_unsigned(0, C_TAG_WIDTH-1));
  end function;

  function make_tagmem_addr(addr : std_logic_vector) return std_logic_vector is
  begin
    return addr(addr'right + (G_LOG2_LINES + G_LOG2_SUBBLOCKS_PER_LINE) - 1 downto addr'right + G_LOG2_SUBBLOCKS_PER_LINE);
  end function;

  function make_datamem_idx_addr(addr : std_logic_vector; idx : std_logic_vector) return std_logic_vector is
  begin
    return make_tagmem_addr(addr) & idx(idx'right + G_LOG2_SUBBLOCKS_PER_LINE - 1 downto idx'right);
  end function;

  function make_datamem_addr(addr : std_logic_vector) return std_logic_vector is
  begin
    return make_datamem_idx_addr(addr, addr);
  end function;

  function make_mem_addr(addr : std_logic_vector; idx : std_logic_vector) return std_logic_vector is
  begin
    return addr(C_WORD_SIZE-1 downto G_LOG2_SUBBLOCKS_PER_LINE+C_MEM_ADR_LSB) & idx;
  end function;

  function make_subblock_idx(addr : std_logic_vector) return std_logic_vector is
  begin
    return addr(G_LOG2_SUBBLOCKS_PER_LINE + C_MEM_ADR_LSB - 1 downto C_MEM_ADR_LSB);
  end function;
begin
  --------------------------------------------------------------------------------------------------
  -- Cache memory instantiation.
  --------------------------------------------------------------------------------------------------

  -- Tag memory.
  tag_ram_1: entity work.ram_dual_port
    generic map (
      WIDTH => C_TAG_WIDTH,
      ADDR_BITS => C_TAG_ADDR_BITS,
      PREFER_DISTRIBUTED => false
    )
    port map (
      i_clk => i_clk,
      i_write_data => s_tagmem_wr_data,
      i_write_addr => s_tagmem_wr_addr,
      i_we => s_tagmem_wr_en,
      i_read_addr => s_tagmem_rd_addr,
      o_read_data => s_tagmem_rd_data
    );

  -- Data memory.
  data_ram_1: entity work.ram_dual_port
    generic map (
      WIDTH => C_MEM_WIDTH,
      ADDR_BITS => C_DATA_ADDR_BITS,
      PREFER_DISTRIBUTED => false
    )
    port map (
      i_clk => i_clk,
      i_write_data => s_datamem_wr_data,
      i_write_addr => s_datamem_wr_addr,
      i_we => s_datamem_wr_en,
      i_read_addr => s_datamem_rd_addr,
      o_read_data => s_datamem_rd_data
    );

  -- Handle forwarding of last written values.
  process(i_clk, i_rst)
  begin
    if i_rst = '1' then
      s_forwarded_tagmem_data <= (others => '0');
      s_use_forwarded_tagmem_data <= '0';
      s_forwarded_datamem_data <= (others => '0');
      s_use_forwarded_datamem_data <= '0';
    elsif rising_edge(i_clk) then
      -- Forward data that is written to tagmem?
      if s_tagmem_wr_en = '1' and s_tagmem_wr_addr = s_tagmem_rd_addr then
        s_use_forwarded_tagmem_data <= '1';
      else
        s_use_forwarded_tagmem_data <= '0';
      end if;
      s_forwarded_tagmem_data <= s_tagmem_wr_data;

      -- Forward data that is written to datamem?
      if s_datamem_wr_en = '1' and s_datamem_wr_addr = s_datamem_rd_addr then
        s_use_forwarded_datamem_data <= '1';
      else
        s_use_forwarded_datamem_data <= '0';
      end if;
      s_forwarded_datamem_data <= s_datamem_wr_data;
    end if;
  end process;

  s_tagmem_rd_data_actual <= s_forwarded_tagmem_data when s_use_forwarded_tagmem_data = '1' else s_tagmem_rd_data;
  s_datamem_rd_data_actual <= s_forwarded_datamem_data when s_use_forwarded_datamem_data = '1' else s_datamem_rd_data;


  --------------------------------------------------------------------------------------------------
  -- Cache lookup (pipelined, stages: IC1, IC2).
  --------------------------------------------------------------------------------------------------

  -- IC1: Send a read request to the cache.
  s_tagmem_rd_addr <= make_tagmem_addr(i_instr_adr);
  s_datamem_rd_addr <= make_datamem_addr(i_instr_adr);

  -- Outputs to the IC2 stage.
  process(i_clk, i_rst)
  begin
    if i_rst = '1' then
      s_ic1_instr_req <= '0';
      s_ic1_instr_adr <= (others => '0');
    elsif rising_edge(i_clk) then
      s_ic1_instr_req <= i_instr_req;
      s_ic1_instr_adr <= i_instr_adr;
    end if;
  end process;

  -- Currently requested subblock-in-cache-line index (LSB:s of address from the instr interface).
  s_requested_subblock_idx <= make_subblock_idx(i_instr_adr);


  --------------------------------------------------------------------------------------------------
  -- Cache line update state machine.
  --------------------------------------------------------------------------------------------------

  -- Should we repeat the current request during the next cycle?
  s_next_repeat_request <= s_start_req and i_mem_stall;

  process(ALL)
  begin
    -- Default values. Optionally overridden in different states.
    s_next_state <= s_state;
    s_next_invalidate_addr <= (others => '-');
    s_next_cache_line_adr <= s_cache_line_adr;
    s_next_cache_line_done <= s_cache_line_done;

    s_tagmem_wr_data <= (others => '-');
    s_tagmem_wr_addr <= (others => '-');
    s_tagmem_wr_en <= '0';
    s_datamem_wr_data <= (others => '-');
    s_datamem_wr_addr <= (others => '-');
    s_datamem_wr_en <= '0';

    s_req_adr <= make_mem_addr(s_ic1_instr_adr, s_subblock_idx);
    s_req_en <= '0';
    o_instr_ack <= '0';
    o_instr_dat <= (others => '-');

    -- Default subblock_idx.
    s_next_subblock_idx <= s_requested_subblock_idx;

    case s_state is
      when RESET =>
        s_next_invalidate_addr <= (others => '0');
        s_next_state <= INVALIDATE;

      when INVALIDATE =>
        s_next_invalidate_addr <= s_invalidate_addr + 1;
        s_tagmem_wr_data <= make_invalid_tag;
        s_tagmem_wr_addr <= std_logic_vector(s_invalidate_addr);
        s_tagmem_wr_en <= '1';

        if s_invalidate_addr = C_MAX_TAG_ADDR then
          s_next_state <= READY;
        end if;

      when READY =>
        if s_invalidate_requested = '1' then
          s_next_invalidate_addr <= (others => '0');
          s_next_state <= INVALIDATE;
        elsif s_ic1_instr_req = '1' then
          -- Check if we had a cache hit or miss.
          if s_tagmem_rd_data_actual = make_valid_tag(s_ic1_instr_adr) then
            -- Cache read hit -> respond to request.
            o_instr_ack <= '1';
            o_instr_dat <= s_datamem_rd_data_actual;
          else
            -- Cache read miss.
            if s_can_start_req = '1' then
              s_req_en <= '1';
              s_next_cache_line_adr <= s_ic1_instr_adr;
              s_next_subblock_idx <= std_logic_vector(unsigned(s_subblock_idx) + 1);
              s_next_state <= UPDATE_CACHE_LINE;
            end if;
          end if;
        end if;

      when UPDATE_CACHE_LINE =>
        -- We use the registered (constant) cache line address during cache line updates.
        s_req_adr <= make_mem_addr(s_cache_line_adr, s_subblock_idx);

        -- Unless we can send a request, stick to the current subblock idx.
        s_next_subblock_idx <= s_subblock_idx;

        if s_cache_line_done = '0' and (s_can_start_req = '1' or s_repeat_request = '1') then
          -- Send a new read request ASAP.
          s_req_en <= '1';
          s_next_subblock_idx <= std_logic_vector(unsigned(s_subblock_idx) + 1);

          -- Final request for this cache line?
          if s_subblock_idx = s_last_subblock_idx then
            s_next_cache_line_done <= '1';
          end if;
        end if;

        -- Collect read results whenever we get an ACK back.
        if s_resp_ack = '1' then
          -- Write the read subblock to the correct index in the cache line.
          s_datamem_wr_data <= s_resp_dat;
          s_datamem_wr_addr <= make_datamem_idx_addr(s_cache_line_adr, s_resp_subblock_idx);
          s_datamem_wr_en <= '1';

          -- Respond to the instr interface when we have a matching address.
          if s_ic1_instr_req = '1' and make_mem_addr(s_cache_line_adr, s_resp_subblock_idx) = s_ic1_instr_adr then
            o_instr_ack <= '1';
            o_instr_dat <= s_resp_dat;
          end if;

          -- Final response for this cache line?
          if s_resp_subblock_idx = s_last_subblock_idx then
            -- Mark the cache line as valid.
            s_tagmem_wr_data <= make_valid_tag(s_cache_line_adr);
            s_tagmem_wr_addr <= make_tagmem_addr(s_cache_line_adr);
            s_tagmem_wr_en <= '1';

            -- Set up signals correctly for moving to READY state.
            s_next_cache_line_done <= '0';
            s_next_subblock_idx <= s_requested_subblock_idx;

            s_next_state <= READY;
          end if;
        end if;

      when others =>
        s_next_state <= RESET;
    end case;
  end process;

  process(i_clk, i_rst)
  begin
    if i_rst = '1' then
      s_invalidate_requested <= '0';
      s_invalidate_addr <= (others => '0');
      s_subblock_idx <= (others => '0');
      s_last_subblock_idx <= (others => '0');
      s_cache_line_adr <= (others => '0');
      s_cache_line_done <= '0';
      s_repeat_request <= '0';
      s_state <= RESET;
    elsif rising_edge(i_clk) then
      -- Sticky remember invalidate requests.
      if i_invalidate = '1' then
        s_invalidate_requested <= '1';
      elsif s_next_state = INVALIDATE then
        s_invalidate_requested <= '0';
      end if;

      -- Update the invalidate line address.
      s_invalidate_addr <= s_next_invalidate_addr;

      -- Update the subblock-within-cache-line counter.
      if s_next_repeat_request = '0' then
        s_subblock_idx <= s_next_subblock_idx;
      end if;
      if s_next_state = READY then
        s_last_subblock_idx <= std_logic_vector(unsigned(s_next_subblock_idx) - 1);
      end if;

      -- Update the cache line address.
      s_cache_line_adr <= s_next_cache_line_adr;

      -- Keep track of whether or not all requestes for a cache line have been sent to memory.
      if s_next_repeat_request = '0' then
        s_cache_line_done <= s_next_cache_line_done;
      end if;

      -- If we got a STALL from the WB interface during the last cycle, we need to repeat the
      -- request (if any).
      s_repeat_request <= s_next_repeat_request;

      -- Move to the next requested state.
      s_state <= s_next_state;
    end if;
  end process;


  --------------------------------------------------------------------------------------------------
  -- We use a FIFO to keep track of ongoing requests. The purpose is to match ACKs from the Wishbone
  -- bus with the requests that we have sent.
  --------------------------------------------------------------------------------------------------

  req_fifo: entity work.fifo
    generic map (
      G_WIDTH => C_REQ_FIFO_WIDTH,
      G_DEPTH => G_REQ_FIFO_DEPTH
    )
    port map (
      i_rst => i_rst,
      i_clk => i_clk,
      i_wr_en => s_fifo_wr_en,
      i_wr_data => s_fifo_wr_data,
      o_full => s_fifo_full,
      i_rd_en => s_fifo_rd_en,
      o_rd_data => s_fifo_rd_data,
      o_empty => s_fifo_empty
    );

  -- Queue memory requests in the FIFO (if we're repeating a request, it's already in the FIFO).
  s_fifo_wr_en <= s_start_req and (not s_repeat_request);
  s_fifo_wr_data <= make_subblock_idx(s_req_adr);

  -- Read from the request FIFO when we get an ACK from the Wishbone bus.
  s_fifo_rd_en <= i_mem_ack and (not s_fifo_empty);

  --------------------------------------------------------------------------------------------------
  -- Memory interface: Send requests.
  --------------------------------------------------------------------------------------------------

  -- Is the FIFO ready to accept new requests?
  s_can_start_req <= not s_fifo_full;

  -- Shall we send a new request?
  s_start_req <= s_req_en and (s_can_start_req or s_repeat_request);

  -- Forward all requests to the main memory interface.
  o_mem_cyc <= s_req_en or (not s_fifo_empty);
  o_mem_stb <= s_start_req;
  o_mem_adr <= s_req_adr;

  --------------------------------------------------------------------------------------------------
  -- Memory interface: Receive request responses.
  --------------------------------------------------------------------------------------------------

  -- Get information about the request that this ACK (if any) corresponds to.
  s_resp_subblock_idx <= s_fifo_rd_data;
  s_resp_ack <= i_mem_ack;
  s_resp_dat <= i_mem_dat;
end rtl;
