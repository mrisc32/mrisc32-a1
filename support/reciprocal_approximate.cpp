//--------------------------------------------------------------------------------------------------
// Copyright (c) 2024 Marcus Geelnard
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the
// authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial
// applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not claim that you wrote
//     the original software. If you use this software in a product, an acknowledgment in the
//     product documentation would be appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
//     being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//--------------------------------------------------------------------------------------------------

#include <array>
#include <bit>
#include <cstdint>
#include <cstdio>
#include <cstring>

constexpr unsigned LUT_BITS = 8U;
constexpr unsigned LUT_SIZE = 1U << LUT_BITS;
constexpr unsigned LUT_RES = 9U;
using lut_entry_t = uint16_t;
using lut_t = std::array<lut_entry_t, LUT_SIZE>;

namespace {

constexpr lut_entry_t to_lut_entry(float const approx) {
  constexpr uint32_t ROUND_BITS = 23U - LUT_RES;
  constexpr uint32_t ROUND_MASK = (1U << ROUND_BITS) - 1U;
  constexpr uint32_t ENTRY_MASK = (1U << LUT_RES) - 1U;

  // Extract LUT_RES bits of the fractional part of the floating-point number.
  auto const approx_i = std::bit_cast<uint32_t>(approx);
  auto const entry_unrounded = (approx_i >> ROUND_BITS) & ENTRY_MASK;

  // Apply rounding (round to nearst).
  auto const need_round = (approx_i & ROUND_MASK) > (ROUND_MASK >> 1U);
  auto const can_round = entry_unrounded != ENTRY_MASK;
  auto const entry_rounded = (need_round && can_round) ? entry_unrounded + 1U : entry_unrounded;

  return static_cast<lut_entry_t>(entry_rounded);
}

constexpr float best_approximation(double const x0, double const x1) {
  constexpr int NUM_SAMPLES = 1024;
  double sum_y = 0.0;
  for (int i = 0; i < NUM_SAMPLES; ++i) {
    double const w = static_cast<double>(i) * (1.0 / static_cast<double>(NUM_SAMPLES));
    double const x = 1.0 + (1.0 - w) * x0 + w * x1;
    double const y = 1.0 / x;
    sum_y += y;
  }
  double const avg_y = sum_y * (1.0 / static_cast<double>(NUM_SAMPLES));
  return static_cast<float>(avg_y);
}

constexpr lut_t init_lut() {
  lut_t lut{};
  double x0 = 0.0;
  for (unsigned k = 0; k < LUT_SIZE; ++k) {
    double const x1 = static_cast<double>(k + 1) * (1.0 / static_cast<double>(LUT_SIZE));
    lut[k] = to_lut_entry(best_approximation(x0, x1));
    x0 = x1;
  }
  return lut;
}

constexpr lut_t LUT = init_lut();

constexpr uint32_t expand_lut_entry(uint32_t const lut_idx) {
  uint32_t const frac0 = static_cast<uint32_t>(LUT[lut_idx]);
  uint32_t frac = frac0;
  uint32_t bits_left = 23U - LUT_RES;
  while (bits_left != 0U) {
    uint32_t const shift = bits_left < LUT_RES ? bits_left : LUT_RES;
    frac = (frac << shift) | (frac0 >> (LUT_RES - shift));
    bits_left -= shift;
  }
  return frac;
}

constexpr float reciprocal_approximation(float x) {
  uint32_t const xi = std::bit_cast<uint32_t>(x);

  // Keep the sign.
  uint32_t const sign = xi & 0x80000000U;

  // Calculate the exponent.
  uint32_t exp = (xi & 0x7f800000U) >> 23;
  exp = ((253 - static_cast<int>(exp)) << 23) & 0x7f800000U;

  // Look up the approximated fraction from the LUT.
  uint32_t const lut_idx = (xi & 0x007fffffU) >> (23 - LUT_BITS);
  uint32_t const frac = expand_lut_entry(lut_idx);

  // Construct the approximate floating point value.
  uint32_t yi = sign | exp | frac;

  return std::bit_cast<float>(yi);
}

constexpr float reciprocal_step(float x, float y) {
  return y * (2.0F - (y * x));
}

constexpr float abs(float x) {
  return x >= 0.0F ? x : -x;
}

template<int N>
void print_bin(uint32_t const x) {
  char buf[N + 1];
  for (int b = 0; b < N; ++b) {
    buf[b] = (x & (1U << (N - b - 1))) != 0U ? '1' : '0';
  }
  buf[N] = 0;
  std::fwrite(buf, 1, N, stdout);
}

void print_lut() {
  std::printf("== LUT =========================================================================\n\n");
  std::printf("  ApproximationLUT: with s_lut_idx select\n");
  std::printf("    s_approx <=\n");
  for (unsigned k = 0; k < LUT.size(); ++k) {
    auto y = static_cast<uint32_t>(LUT[k]);
    std::printf("      \"");
    print_bin<LUT_RES>(y);
    std::printf("\" when \"");
    print_bin<LUT_BITS>(k);
    std::printf("\",\n");
  }
  std::printf("      (others => '-') when others;\n\n");
}

void test() {
  std::printf("== TEST ========================================================================\n\n");
  float values[] = {1.0F, 2.0F, -2.5F, 3.0F, -21.23F, 100.3F, 3.141592654F, 1.9999999F, 1234567.0F};
  for (int i = 0; i < static_cast<int>(sizeof(values) / sizeof(values[0])); ++i) {
    float const x = values[i];
    float const y0 = reciprocal_approximation(x);
    float const y1 = reciprocal_step(x, y0);
    float const y2 = reciprocal_step(x, y1);
    float const y3 = reciprocal_step(x, y2);
    float const actual = 1.0F / x;
    float const e0 = abs((y0 - actual) / actual);
    float const e1 = abs((y1 - actual) / actual);
    float const e2 = abs((y2 - actual) / actual);
    std::printf("1/%.10f ~=\t%.10f\t%.10f\t%.10f\t%.10f\t(actual: %.10f\te0=%.4e\te1=%.4e\te2=%.4e)\n", x, y0, y1, y2, y3, actual, e0, e1, e2);
  }
  std::printf("\n");
}

}  // namespace

int main(void)
{
  test();
  print_lut();
}

