----------------------------------------------------------------------------------------------------
-- Copyright (c) 2023 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.types.all;
use work.config.all;

entity dcache_tb is
end dcache_tb;

architecture behavioral of dcache_tb is
  constant C_RAM_ADR_WIDTH : integer := 12;  -- 2^12 = 4096 words = 16 KiB

  -- Control signals.
  signal s_clk : std_logic;
  signal s_rst : std_logic;
  signal s_invalidate : std_logic;
  signal s_flush : std_logic;

  -- DCache data interface (slave).
  signal s_data_req : std_logic;
  signal s_data_adr : std_logic_vector(C_WORD_SIZE-1 downto 2);
  signal s_data_dat_w : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal s_data_we : std_logic;
  signal s_data_sel : std_logic_vector(C_WORD_SIZE/8-1 downto 0);
  signal s_data_dat_r : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal s_data_ack : std_logic;
  signal s_data_busy : std_logic;

  -- DCache memory interface (WB master).
  signal s_mem_cyc : std_logic;
  signal s_mem_stb : std_logic;
  signal s_mem_adr : std_logic_vector(C_WORD_SIZE-1 downto 2);
  signal s_mem_dat_w : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal s_mem_we : std_logic;
  signal s_mem_sel : std_logic_vector(C_WORD_SIZE/8-1 downto 0);
  signal s_mem_dat_r : std_logic_vector(C_WORD_SIZE-1 downto 0);
  signal s_mem_ack : std_logic;
  signal s_mem_stall : std_logic;
  signal s_mem_err : std_logic;

  -- RAM memory interface (WB slave).
  signal s_ram_adr : std_logic_vector(C_RAM_ADR_WIDTH-1 downto 0);
begin
  -- Instantiate the DUT.
  -- We configure it to a small size: 256 bytes (64 words).
  dut_0: entity work.dcache
    generic map (
      G_LOG2_LINES => 3,
      G_LOG2_SUBBLOCKS_PER_LINE => 3,
      G_LOG2_VC_ENTRIES => 2,
      G_REQ_FIFO_DEPTH => 8
    )
    port map (
      -- Control signals.
      i_clk => s_clk,
      i_rst => s_rst,
      i_invalidate => s_invalidate,
      i_flush => s_flush,

      -- Data interface (slave).
      i_data_req => s_data_req,
      i_data_adr => s_data_adr,
      i_data_dat => s_data_dat_w,
      i_data_we => s_data_we,
      i_data_sel => s_data_sel,
      o_data_dat => s_data_dat_r,
      o_data_ack => s_data_ack,
      o_data_busy => s_data_busy,

      -- Memory interface (WB master).
      o_mem_cyc => s_mem_cyc,
      o_mem_stb => s_mem_stb,
      o_mem_adr => s_mem_adr,
      o_mem_dat => s_mem_dat_w,
      o_mem_we => s_mem_we,
      o_mem_sel => s_mem_sel,
      i_mem_dat => s_mem_dat_r,
      i_mem_ack => s_mem_ack,
      i_mem_stall => s_mem_stall,
      i_mem_err => s_mem_err
    );

  -- Instantiate the RAM.
  s_ram_adr <= s_mem_adr(C_RAM_ADR_WIDTH+2-1 downto 2);

  ram_0: entity work.ram_model
    generic map (
      G_DAT_WIDTH => C_WORD_SIZE,
      G_ADR_WIDTH => C_RAM_ADR_WIDTH,
      G_LATENCY => 16
    )
    port map (
      -- Control signals.
      i_clk => s_clk,
      i_rst => s_rst,

      -- Memory interface (WB slave).
      i_cyc => s_mem_cyc,
      i_stb => s_mem_stb,
      i_adr => s_ram_adr,
      i_dat => s_mem_dat_w,
      i_we => s_mem_we,
      i_sel => s_mem_sel,
      o_dat => s_mem_dat_r,
      o_ack => s_mem_ack,
      o_stall => s_mem_stall,
      o_err => s_mem_err
    );

  process
    -- Patterns to apply.
    type T_REQ is (INV, RD, WR, NOP);

    type T_MEMOP is record
      -- Memory operation.
      req : T_REQ;
      adr : std_logic_vector(C_WORD_SIZE-1 downto 0);
      dat_w : std_logic_vector(C_WORD_SIZE-1 downto 0);
      sel : std_logic_vector(C_WORD_SIZE/8-1 downto 0);

      -- Expected result.
      dat_r : std_logic_vector(C_WORD_SIZE-1 downto 0);
    end record;

    type T_MEMOP_ARRAY is array (natural range <>) of T_MEMOP;

    constant C_MEMOP_ARRAY : T_MEMOP_ARRAY := (
        -- ==[ Req ]========================  ==[ Result ]==

        -- Write + read word (cache miss).
        (WR, x"00000018",x"12345678","1111",  x"--------"),
        (WR, x"0000001c",x"87654321","1111",  x"--------"),
        (RD, x"00000018",x"--------","1111",  x"12345678"),
        (RD, x"0000001c",x"--------","1111",  x"87654321"),

        -- Read word (cache hit).
        (RD, x"00000018",x"--------","1111",  x"12345678"),
        (RD, x"0000001c",x"--------","1111",  x"87654321"),

        -- Write + read word (cache hit).
        (WR, x"00000018",x"deadbeef","1111",  x"--------"),
        (WR, x"0000001c",x"cafebabe","1111",  x"--------"),
        (RD, x"00000018",x"--------","1111",  x"deadbeef"),
        (RD, x"0000001c",x"--------","1111",  x"cafebabe"),

        -- Write + read word (cache hit, data forward).
        (WR, x"00000018",x"aaaaaaaa","1111",  x"--------"),
        (RD, x"00000018",x"--------","1111",  x"aaaaaaaa"),

        -- Write + read half-word (cache hit).
        (WR, x"00000018",x"9999ffff","1100",  x"--------"),
        (WR, x"00000018",x"ffff9999","0011",  x"--------"),
        (RD, x"00000018",x"--------","1111",  x"99999999"),

        -- Write + read byte (cache miss).
        (WR, x"00000020",x"11ffffff","1000",  x"--------"),
        (WR, x"00000020",x"ff22ffff","0100",  x"--------"),
        (WR, x"00000020",x"ffff33ff","0010",  x"--------"),
        (WR, x"00000020",x"ffffff44","0001",  x"--------"),
        (RD, x"00000020",x"--------","1111",  x"11223344"),

        -- Write + read byte (cache hit).
        (WR, x"00000024",x"11ffffff","1000",  x"--------"),
        (RD, x"00000024",x"--------","1111",  x"11------"),
        (WR, x"00000024",x"ff22ffff","0100",  x"--------"),
        (RD, x"00000024",x"--------","1111",  x"1122----"),
        (WR, x"00000024",x"ffff33ff","0010",  x"--------"),
        (RD, x"00000024",x"--------","1111",  x"112233--"),
        (WR, x"00000024",x"ffffff44","0001",  x"--------"),
        (RD, x"00000024",x"--------","1111",  x"11223344"),

        -- Write + read word (cache eviction).
        (WR, x"00000018",x"32100123","1111",  x"--------"),
        (WR, x"00000218",x"01233210","1111",  x"--------"),
        (NOP,x"00000000",x"--------","1111",  x"--------"),
        (NOP,x"00000000",x"--------","1111",  x"--------"),
        (RD, x"00000018",x"--------","1111",  x"32100123"),  -- (hit)
        (RD, x"00000218",x"--------","1111",  x"01233210"),  -- (miss -> update)

        -- Read miss with victim hit.
        (RD, x"00000018",x"--------","1111",  x"32100123"),  -- (miss -> swap)
        (RD, x"00000218",x"--------","1111",  x"01233210"),  -- (miss -> swap)

        -- Write to the victim cache.
        (WR, x"00000018",x"5a5a5a5a","1100",  x"--------"),  -- (update in victim cache - slow)
        (RD, x"00000018",x"--------","1111",  x"5a5a0123"),  -- (miss -> swap)
        (WR, x"0000021c",x"a5a5a5a5","1111",  x"--------"),  -- (update in victim cache - fast)
        (RD, x"0000021c",x"--------","1111",  x"a5a5a5a5"),  -- (miss -> swap)

        -- INVALIDATE.
        (INV,x"00000000",x"--------","1111",  x"--------"),

        -- Fill all the victim cache ways.
        (WR, x"00000003",x"abcd0001","1111",  x"--------"),
        (WR, x"00000103",x"abcd0002","1111",  x"--------"),
        (WR, x"00000203",x"abcd0003","1111",  x"--------"),
        (WR, x"00000303",x"abcd0004","1111",  x"--------"),
        (RD, x"00000003",x"--------","1111",  x"abcd0001"),
        (RD, x"00000103",x"--------","1111",  x"abcd0002"),
        (RD, x"00000203",x"--------","1111",  x"abcd0003"),
        (RD, x"00000303",x"--------","1111",  x"abcd0004"),

        -- Update entires in the victim cache.
        (WR, x"00000003",x"dcba0001","11--",  x"--------"),
        (WR, x"00000103",x"dcba0002","11--",  x"--------"),
        (WR, x"00000203",x"dcba0003","11--",  x"--------"),
        (WR, x"00000303",x"dcba0004","11--",  x"--------"),
        (RD, x"00000003",x"--------","1111",  x"dcba0001"),
        (RD, x"00000103",x"--------","1111",  x"dcba0002"),
        (RD, x"00000203",x"--------","1111",  x"dcba0003"),
        (RD, x"00000303",x"--------","1111",  x"dcba0004"),

        -- Exhaust the victim cache (evict victim cache entries).
        (WR, x"00000403",x"dcba0005","1111",  x"--------"),
        (RD, x"00000003",x"--------","1111",  x"dcba0001"),
        (RD, x"00000103",x"--------","1111",  x"dcba0002"),
        (RD, x"00000203",x"--------","1111",  x"dcba0003"),
        (RD, x"00000303",x"--------","1111",  x"dcba0004"),
        (RD, x"00000403",x"--------","1111",  x"dcba0005"),

        -- INVALIDATE.
        (INV,x"00000000",x"--------","1111",  x"--------"),

        -- memcpy words (alternating cache lines).
        (WR, x"00000040",x"11111111","1111",  x"--------"),
        (WR, x"00000044",x"22222222","1111",  x"--------"),
        (WR, x"00000048",x"33333333","1111",  x"--------"),
        (WR, x"0000004c",x"44444444","1111",  x"--------"),

        (RD, x"00000040",x"--------","1111",  x"11111111"),
        (WR, x"00000060",x"11111111","1111",  x"--------"),
        (RD, x"00000044",x"--------","1111",  x"22222222"),
        (WR, x"00000064",x"22222222","1111",  x"--------"),
        (RD, x"00000048",x"--------","1111",  x"33333333"),
        (WR, x"00000068",x"33333333","1111",  x"--------"),
        (RD, x"0000004c",x"--------","1111",  x"44444444"),
        (WR, x"0000006c",x"44444444","1111",  x"--------"),

        (RD, x"00000060",x"--------","1111",  x"11111111"),
        (RD, x"00000064",x"--------","1111",  x"22222222"),
        (RD, x"00000068",x"--------","1111",  x"33333333"),
        (RD, x"0000006c",x"--------","1111",  x"44444444"),

        -- Fill up the request FIFO (cache hits).
        (RD, x"00000040",x"--------","1111",  x"11111111"),

        (WR, x"00000040",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000044",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000048",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000040",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000044",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000048",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000040",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000044",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000048",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000040",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000044",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000048",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000040",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000044",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000048",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000040",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000044",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000048",x"5a5a5a5a","1111",  x"--------"),
        (WR, x"00000040",x"a5a5a5a5","1111",  x"--------"),
        (WR, x"00000044",x"5a5a5a5a","1111",  x"--------"),

        (RD, x"00000040",x"--------","1111",  x"a5a5a5a5"),

        -- Fill up the request FIFO (cache misses).
        (WR, x"00000080",x"0000000f","1111",  x"--------"),
        (WR, x"00000084",x"000000ff","1111",  x"--------"),
        (WR, x"00000088",x"00000fff","1111",  x"--------"),
        (WR, x"00000080",x"0000ffff","1111",  x"--------"),
        (WR, x"00000084",x"000fffff","1111",  x"--------"),
        (WR, x"00000088",x"00ffffff","1111",  x"--------"),
        (WR, x"00000080",x"0fffffff","1111",  x"--------"),
        (WR, x"00000084",x"ffffffff","1111",  x"--------"),
        (WR, x"00000088",x"fffffff0","1111",  x"--------"),
        (WR, x"00000080",x"ffffff00","1111",  x"--------"),
        (WR, x"00000084",x"fffff000","1111",  x"--------"),
        (WR, x"00000088",x"ffff0000","1111",  x"--------"),
        (WR, x"00000080",x"fff00000","1111",  x"--------"),
        (WR, x"00000084",x"ff000000","1111",  x"--------"),
        (WR, x"00000088",x"f0000000","1111",  x"--------"),
        (WR, x"00000080",x"00000005","1111",  x"--------"),
        (WR, x"00000084",x"00000055","1111",  x"--------"),
        (WR, x"00000088",x"00000555","1111",  x"--------"),
        (WR, x"00000080",x"00005555","1111",  x"--------"),
        (WR, x"00000084",x"00055555","1111",  x"--------"),

        (RD, x"00000080",x"--------","1111",  x"00005555"),

        -- Prepare for random access reads.
        (WR, x"00000000",x"00000001","1111",  x"--------"),
        (WR, x"00000024",x"00000002","1111",  x"--------"),
        (WR, x"00000048",x"00000003","1111",  x"--------"),
        (WR, x"0000006c",x"00000004","1111",  x"--------"),
        (WR, x"00000080",x"00000005","1111",  x"--------"),
        (WR, x"000000a4",x"00000006","1111",  x"--------"),
        (WR, x"000000c8",x"00000007","1111",  x"--------"),
        (WR, x"000000ec",x"00000008","1111",  x"--------"),
        (WR, x"00000100",x"00000009","1111",  x"--------"),
        (WR, x"00000124",x"0000000a","1111",  x"--------"),
        (WR, x"00000148",x"0000000b","1111",  x"--------"),
        (WR, x"0000016c",x"0000000c","1111",  x"--------"),
        (WR, x"00000180",x"0000000d","1111",  x"--------"),
        (WR, x"000001a4",x"0000000e","1111",  x"--------"),
        (WR, x"000001c8",x"0000000f","1111",  x"--------"),
        (WR, x"000001ec",x"00000010","1111",  x"--------"),

        -- Random access reads - cache misses.
        (INV,x"00000000",x"--------","1111",  x"--------"),

        (RD, x"000000ec",x"00000008","1111",  x"00000008"),
        (RD, x"0000016c",x"0000000c","1111",  x"0000000c"),
        (RD, x"00000124",x"0000000a","1111",  x"0000000a"),
        (RD, x"000000a4",x"00000006","1111",  x"00000006"),
        (RD, x"00000100",x"00000009","1111",  x"00000009"),
        (RD, x"000000c8",x"00000007","1111",  x"00000007"),
        (RD, x"0000006c",x"00000004","1111",  x"00000004"),
        (RD, x"000001ec",x"00000010","1111",  x"00000010"),
        (RD, x"00000080",x"00000005","1111",  x"00000005"),
        (RD, x"000001a4",x"0000000e","1111",  x"0000000e"),
        (RD, x"00000048",x"00000003","1111",  x"00000003"),
        (RD, x"000001c8",x"0000000f","1111",  x"0000000f"),
        (RD, x"00000148",x"0000000b","1111",  x"0000000b"),
        (RD, x"00000000",x"00000001","1111",  x"00000001"),
        (RD, x"00000180",x"0000000d","1111",  x"0000000d"),
        (RD, x"00000024",x"00000002","1111",  x"00000002"),

        -- Random access reads - mixed cache hits & victim cache hits.
        (RD, x"00000080",x"00000005","1111",  x"00000005"),
        (RD, x"000001a4",x"0000000e","1111",  x"0000000e"),
        (RD, x"00000048",x"00000003","1111",  x"00000003"),
        (RD, x"000001c8",x"0000000f","1111",  x"0000000f"),
        (RD, x"00000148",x"0000000b","1111",  x"0000000b"),
        (RD, x"00000000",x"00000001","1111",  x"00000001"),
        (RD, x"00000180",x"0000000d","1111",  x"0000000d"),
        (RD, x"00000024",x"00000002","1111",  x"00000002"),

        (NOP,x"00000000",x"--------","1111",  x"--------")
      );

    constant C_MAX_CYCLE_CNT : integer := 2000;

    -- State.
    variable v_cycle_cnt : integer;
    variable v_req_idx : integer;
    variable v_res_idx : integer;
    variable v_req_op : T_MEMOP;
    variable v_res_op : T_MEMOP;
  begin
    -- Clear all input signals.
    s_clk <= '0';
    s_rst <= '0';
    s_invalidate <= '0';
    s_flush <= '0';
    s_data_req <= '0';
    s_data_adr <= (others => '0');
    s_data_dat_w <= (others => '0');
    s_data_we <= '0';
    s_data_sel <= (others => '0');

    -- Start by resetting the DUT (to have a defined state).
    s_rst <= '1';
    s_clk <= '0';
    wait for 1 ns;
    s_clk <= '1';
    wait for 1 ns;
    s_clk <= '0';
    s_rst <= '0';
    wait for 1 ns;

    -- Test all the patterns in the pattern array.
    v_cycle_cnt := 0;
    v_req_idx := C_MEMOP_ARRAY'low;
    v_res_idx := C_MEMOP_ARRAY'low;
    while v_res_idx <= C_MEMOP_ARRAY'high and v_cycle_cnt < C_MAX_CYCLE_CNT loop
      -- Positivie clock flank (tick registers).
      s_clk <= '1';
      wait until s_clk = '1';

      -- Make a new request?
      if v_req_idx <= C_MEMOP_ARRAY'high then
        v_req_op := C_MEMOP_ARRAY(v_req_idx);
        if v_req_op.req = INV then
          s_data_req <= '0';
          s_data_we <= '0';
          s_invalidate <= '1';
        elsif v_req_op.req = RD then
          s_data_req <= '1';
          s_data_we <= '0';
          s_invalidate <= '0';
        elsif v_req_op.req = WR then
          s_data_req <= '1';
          s_data_we <= '1';
          s_invalidate <= '0';
        else
          s_data_req <= '0';
          s_data_we <= '0';
          s_invalidate <= '0';
        end if;
        s_data_adr <= v_req_op.adr(C_WORD_SIZE-1 downto 2);
        s_data_dat_w <= v_req_op.dat_w;
        s_data_sel <= v_req_op.sel;
      else
        s_data_req <= '0';
        s_data_we <= '0';
        s_invalidate <= '0';
        s_data_adr <= (others => '0');
        s_data_dat_w <= (others => '0');
        s_data_sel <= (others => '0');
      end if;

      -- Wait for the results.
      wait for 1 ns;

      -- Check the outputs.
      v_res_op := C_MEMOP_ARRAY(v_res_idx);
      assert std_match(s_data_dat_r, v_res_op.dat_r)
        report "Bad result (" & integer'image(v_res_idx) & "):" & lf &
               "  data_dat_r = " & to_string(s_data_dat_r) & lf &
               "  expected   = " & to_string(v_res_op.dat_r)
            severity error;

      -- Advance the result counter if we got an ack from the cache (or if the req was a INV/NOP).
      if v_res_op.req = INV or v_res_op.req = NOP or s_data_ack = '1' then
        v_res_idx := v_res_idx + 1;
      end if;

      -- Advance the request counter if the cache could handle the request.
      if s_data_busy = '0' then
        v_req_idx := v_req_idx + 1;
      end if;

      -- Tick the clock.
      s_clk <= '0';
      wait for 1 ns;

      v_cycle_cnt := v_cycle_cnt + 1;
    end loop;

    assert v_cycle_cnt < C_MAX_CYCLE_CNT report "Test stuck (missing ack?)" severity error;
    assert false report "End of test" severity note;
    --  Wait forever; this will finish the simulation.
    wait;
  end process;
end behavioral;

