----------------------------------------------------------------------------------------------------
-- Copyright (c) 2023 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- A RAM model with a Wishbone interface, for use in tests.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ram_model is
  generic(
    G_DAT_WIDTH : integer;
    G_ADR_WIDTH : integer;
    G_STALL_INTERVAL : integer := 7;
    G_STALL_LEN : integer := 2;
    G_LATENCY : integer := 0
  );
  port(
    -- Control signals.
    i_clk : in std_logic;
    i_rst : in std_logic;

    -- Memory interface (WB slave).
    i_cyc : in std_logic;
    i_stb : in std_logic;
    i_adr : in std_logic_vector(G_ADR_WIDTH-1 downto 0);
    i_dat : in std_logic_vector(G_DAT_WIDTH-1 downto 0);
    i_we : in std_logic;
    i_sel : in std_logic_vector(G_DAT_WIDTH/8-1 downto 0);
    o_dat : out std_logic_vector(G_DAT_WIDTH-1 downto 0);
    o_ack : out std_logic;
    o_stall : out std_logic;
    o_err : out std_logic
  );
end ram_model;

architecture behavioral of ram_model is
  --------------------------------------------------------------------------------------------------
  -- RAM types and signals.
  --------------------------------------------------------------------------------------------------

  constant C_BYTES_PER_WORD : integer := G_DAT_WIDTH / 8;
  constant C_NUM_WORDS : integer := 2**G_ADR_WIDTH;

  subtype T_WORD is std_logic_vector(G_DAT_WIDTH-1 downto 0);

  type T_RAM_ARRAY is array (0 to C_NUM_WORDS-1) of T_WORD;
  signal s_ram_array : T_RAM_ARRAY := (others => (others => '0'));

  signal s_stall_cnt : integer;
  signal s_ignore_req : boolean;


  -- Request queue (latency pipe).

  type T_REQUEST is record
    req : std_logic;
    adr : std_logic_vector(G_ADR_WIDTH-1 downto 0);
    dat : std_logic_vector(G_DAT_WIDTH-1 downto 0);
    we : std_logic;
    sel : std_logic_vector(G_DAT_WIDTH/8-1 downto 0);
  end record;

  constant C_REQUEST_ARRAY_MAX_IDX : integer := maximum(0, G_LATENCY - 1);
  type T_REQUEST_ARRAY is array (0 to C_REQUEST_ARRAY_MAX_IDX) of T_REQUEST;
  signal s_request_array : T_REQUEST_ARRAY := (others => (req => '0',
                                                          adr => (others => '0'),
                                                          dat => (others => '0'),
                                                          we => '0',
                                                          sel => (others => '0')
                                                         ));
  signal s_request_array_pos : integer range 0 to C_REQUEST_ARRAY_MAX_IDX := 0;

  --------------------------------------------------------------------------------------------------
  -- Helper functions.
  --------------------------------------------------------------------------------------------------

  function mix_words(sel : std_logic_vector; old_data : std_logic_vector; new_data : std_logic_vector) return std_logic_vector is
    variable v_data : std_logic_vector(G_DAT_WIDTH-1 downto 0);
    variable v_idx : integer;
  begin
    for k in sel'range loop
      v_idx := 8 * k;
      if sel(k) = '1' then
        v_data(v_idx+7 downto v_idx) := new_data(v_idx+7 downto v_idx);
      else
        v_data(v_idx+7 downto v_idx) := old_data(v_idx+7 downto v_idx);
      end if;
    end loop;
    return v_data;
  end function;

begin
  process(i_clk, i_rst)
    variable v_adr : integer;
    variable v_dat : T_WORD;
    variable v_do_stall : boolean;
    variable v_incoming_request : T_REQUEST;
    variable v_request : T_REQUEST;
  begin
    if i_rst = '1' then
      o_dat <= (others => '-');
      o_ack <= '0';
      o_stall <= '0';
      o_err <= '0';
      s_stall_cnt <= 0;
      s_ignore_req <= false;
      s_request_array_pos <= 0;
    elsif rising_edge(i_clk) then
      -- Should we stall?
      v_do_stall := (s_stall_cnt > (G_STALL_INTERVAL - G_STALL_LEN));
      if s_stall_cnt = G_STALL_INTERVAL then
        s_stall_cnt <= 0;
      else
        s_stall_cnt <= s_stall_cnt + 1;
      end if;
      o_stall <= '1' when v_do_stall else '0';

      -- Ignore any request that is clocked in on the next cycle.
      s_ignore_req <= v_do_stall;

      -- Get the incoming request.
      if i_cyc = '1' and i_stb = '1' and not s_ignore_req then
        v_incoming_request.req := '1';
      else
        v_incoming_request.req := '0';
      end if;
      v_incoming_request.adr := i_adr;
      v_incoming_request.dat := i_dat;
      v_incoming_request.we := i_we;
      v_incoming_request.sel := i_sel;

      if G_LATENCY = 0 then
        v_request := v_incoming_request;
      else
        -- Pop the next request from the queue.
        if s_request_array_pos = C_REQUEST_ARRAY_MAX_IDX then
          v_request := s_request_array(0);
        else
          v_request := s_request_array(s_request_array_pos + 1);
        end if;

        -- Push the incoming request.
        s_request_array(s_request_array_pos) <= v_incoming_request;

        -- Tick the latency queue position.
        if s_request_array_pos = C_REQUEST_ARRAY_MAX_IDX then
          s_request_array_pos <= 0;
        else
          s_request_array_pos <= s_request_array_pos + 1;
        end if;
      end if;

      -- Did we have a request?
      if v_request.req = '1' then
        v_adr := to_integer(unsigned(v_request.adr));
        v_dat := s_ram_array(v_adr);
        if v_request.we = '1' then
          s_ram_array(v_adr) <= mix_words(v_request.sel, v_dat, v_request.dat);
          o_dat <= (others => '-');
        else
          o_dat <= v_dat;
        end if;
        o_ack <= '1';
      else
        o_dat <= (others => '-');
        o_ack <= '0';
      end if;

      -- We never error.
      o_err <= '0';
    end if;
  end process;
end behavioral;
