----------------------------------------------------------------------------------------------------
-- Copyright (c) 2024 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.types.all;
use work.config.all;

--  A testbench has no ports.
entity frsqrta_tb is
end frsqrta_tb;

architecture behav of frsqrta_tb is
  constant WIDTH : integer := 32;
  constant EXP_BITS : integer := 8;
  constant EXP_BIAS : integer := 127;
  constant FRACT_BITS : integer := 23;

  signal s_src : std_logic_vector(WIDTH-1 downto 0);
  signal s_result : std_logic_vector(WIDTH-1 downto 0);

  signal s_exponent : std_logic_vector(EXP_BITS-1 downto 0);
  signal s_significand : std_logic_vector(FRACT_BITS downto 0);
  signal s_props : T_FLOAT_PROPS;
begin
  --  Component instantiation.
  frsqrta_0: entity work.frsqrta
    generic map (
      WIDTH => WIDTH,
      EXP_BITS => EXP_BITS,
      EXP_BIAS => EXP_BIAS,
      FRACT_BITS => FRACT_BITS
    )
    port map (
      i_props => s_props,
      i_exponent => s_exponent,
      i_significand => s_significand,
      o_result => s_result
    );

  -- Instantiate a de-composer.
  float_decompose_0: entity work.float_decompose
    generic map (
      WIDTH => WIDTH,
      EXP_BITS => EXP_BITS,
      FRACT_BITS => FRACT_BITS
    )
    port map (
      i_src => s_src,
      o_exponent => s_exponent,
      o_significand => s_significand,
      o_props => s_props
    );

  process
    --  The patterns to apply.
    type T_PATTERN is record
      -- Inputs
      src : std_logic_vector(WIDTH-1 downto 0);

      -- Expected outputs
      result : std_logic_vector(WIDTH-1 downto 0);
    end record;
    type T_PATTERN_ARRAY is array (natural range <>) of T_PATTERN;
    constant PATTERNS : T_PATTERN_ARRAY := (
        ("00000000000000000000000000000000",  -- 0.0
         "01111111100000000000000000000000"), -- Inf

        ("01111111100000000000000000000000",  -- Inf
         "00000000000000000000000000000000"), -- 0.0

        ("01111111110000000000000000000000",  -- NaN
         "01111111111111111111111111111111"), -- NaN

        ("10111111100000000000000000000000",  -- -1.0
         "11111111111111111111111111111111"), -- -NaN

        ("00111111100000000000000000000000",  -- 1.0
         "00111111011111111011111111011111"), -- 0.9990215

        ("01000001000000000000000000000000",  -- 8.0
         "00111110101101001101101001101101"), -- 0.35322896

        ("01000000010010010000111111011011",  -- 3.1415927
         "00111111000100000100100000100100"), -- 0.5636008

        ("01111110011111111111111111111111",  -- 8.5070587e37
         "00100000000000000100000000100000"), -- 1.0863239e-19

        ("00000000100000000000000000000000",  -- 1.1754944e-38
         "01011110111111111011111111011111"), -- 9.2143467e18

        ("01111111011010110111100110100011",  -- 3.1300015e38
         "00011111100001011000001011000001")  -- 5.6544005e-20
      );
  begin
    -- Test all the patterns in the pattern array.
    for i in PATTERNS'range loop
      --  Set the inputs.
      s_src <= PATTERNS(i).src;

      --  Wait for the results.
      wait for 1 ns;

      --  Check the outputs.
      assert s_result = PATTERNS(i).result
        report "Bad frsqrta result (" & to_string(i) & ")" & lf &
               "  src=" & to_string(s_src) & lf &
               "  r=" & to_string(s_result) & lf &
               " (e=" & to_string(PATTERNS(i).result) & ")"
            severity error;
    end loop;
    assert false report "End of test" severity note;
    --  Wait forever; this will finish the simulation.
    wait;
  end process;
end behav;
