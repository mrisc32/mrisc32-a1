----------------------------------------------------------------------------------------------------
-- Copyright (c) 2018 Marcus Geelnard
--
-- This software is provided 'as-is', without any express or implied warranty. In no event will the
-- authors be held liable for any damages arising from the use of this software.
--
-- Permission is granted to anyone to use this software for any purpose, including commercial
-- applications, and to alter it and redistribute it freely, subject to the following restrictions:
--
--  1. The origin of this software must not be misrepresented; you must not claim that you wrote
--     the original software. If you use this software in a product, an acknowledgment in the
--     product documentation would be appreciated but is not required.
--
--  2. Altered source versions must be plainly marked as such, and must not be misrepresented as
--     being the original software.
--
--  3. This notice may not be removed or altered from any source distribution.
----------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config.all;
use work.types.all;

entity clz32_tb is
end clz32_tb;

architecture behavioral of clz32_tb is
  signal s_op : T_ALU_OP;
  signal s_src : std_logic_vector(31 downto 0);
  signal s_src_rev : std_logic_vector(31 downto 0);
  signal s_packed_mode : T_PACKED_MODE;
  signal s_result : std_logic_vector(31 downto 0);
begin
  rev32_0: entity work.rev32
    generic map (
      CONFIG => C_CORE_CONFIG_FULL
    )
    port map (
      i_src => s_src,
      i_packed_mode => s_packed_mode,
      o_result => s_src_rev
    );

  clz32_0: entity work.clz32
    generic map (
      CONFIG => C_CORE_CONFIG_FULL
    )
    port map (
      i_op => s_op,
      i_src => s_src,
      i_src_rev => s_src_rev,
      i_packed_mode => s_packed_mode,
      o_result => s_result
    );

  process
    type T_PATTERN is record
      -- Inputs
      op : T_ALU_OP;
      packed_mode : T_PACKED_MODE;
      src : std_logic_vector(31 downto 0);

      -- Expected outputs
      result : std_logic_vector(31 downto 0);
    end record;
    type T_PATTERN_ARRAY is array (natural range <>) of T_PATTERN;
    constant C_PATTERNS : T_PATTERN_ARRAY := (
        (C_ALU_CLZ, C_PACKED_NONE,      B"00000000_00000000_00000000_00000000",  X"00000020"),
        (C_ALU_CLZ, C_PACKED_HALF_WORD, B"00000000_00000000_00000000_00000000",  X"00100010"),
        (C_ALU_CLZ, C_PACKED_BYTE,      B"00000000_00000000_00000000_00000000",  X"08080808"),
        (C_ALU_CLZ, C_PACKED_NONE,      B"00000000_00011100_00000000_01111111",  X"0000000b"),
        (C_ALU_CLZ, C_PACKED_HALF_WORD, B"00000000_00011100_00000000_01111111",  X"000b0009"),
        (C_ALU_CLZ, C_PACKED_BYTE,      B"00000000_00011100_00000000_01111111",  X"08030801"),
        (C_ALU_CLZ, C_PACKED_NONE,      B"11111111_11111111_11111111_11111111",  X"00000000"),
        (C_ALU_CLZ, C_PACKED_HALF_WORD, B"11111111_11111111_11111111_11111111",  X"00000000"),
        (C_ALU_CLZ, C_PACKED_BYTE,      B"11111111_11111111_11111111_11111111",  X"00000000"),

        (C_ALU_CTZ, C_PACKED_NONE,      B"00000000_00000000_00000000_00000000",  X"00000020"),
        (C_ALU_CTZ, C_PACKED_HALF_WORD, B"00000000_00000000_00000000_00000000",  X"00100010"),
        (C_ALU_CTZ, C_PACKED_BYTE,      B"00000000_00000000_00000000_00000000",  X"08080808"),
        (C_ALU_CTZ, C_PACKED_NONE,      B"00000000_00011100_01000000_01110000",  X"00000004"),
        (C_ALU_CTZ, C_PACKED_HALF_WORD, B"00000000_00011100_01000000_01110000",  X"00020004"),
        (C_ALU_CTZ, C_PACKED_BYTE,      B"00000000_00011100_01000000_01110000",  X"08020604"),
        (C_ALU_CTZ, C_PACKED_NONE,      B"11111111_11111111_11111111_11111111",  X"00000000"),
        (C_ALU_CTZ, C_PACKED_HALF_WORD, B"11111111_11111111_11111111_11111111",  X"00000000"),
        (C_ALU_CTZ, C_PACKED_BYTE,      B"11111111_11111111_11111111_11111111",  X"00000000"),

        (C_ALU_CLO, C_PACKED_NONE,      B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CLO, C_PACKED_HALF_WORD, B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CLO, C_PACKED_BYTE,      B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CLO, C_PACKED_NONE,      B"11111100_00011100_11000000_11110000",  X"00000006"),
        (C_ALU_CLO, C_PACKED_HALF_WORD, B"11111100_00011100_11000000_11110000",  X"00060002"),
        (C_ALU_CLO, C_PACKED_BYTE,      B"11111100_00011100_11000000_11110000",  X"06000204"),
        (C_ALU_CLO, C_PACKED_NONE,      B"11111111_11111111_11111111_11111111",  X"00000020"),
        (C_ALU_CLO, C_PACKED_HALF_WORD, B"11111111_11111111_11111111_11111111",  X"00100010"),
        (C_ALU_CLO, C_PACKED_BYTE,      B"11111111_11111111_11111111_11111111",  X"08080808"),

        (C_ALU_CTO, C_PACKED_NONE,      B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CTO, C_PACKED_HALF_WORD, B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CTO, C_PACKED_BYTE,      B"00000000_00000000_00000000_00000000",  X"00000000"),
        (C_ALU_CTO, C_PACKED_NONE,      B"00000011_00111101_11111111_00000111",  X"00000003"),
        (C_ALU_CTO, C_PACKED_HALF_WORD, B"00000011_00111101_11111111_00000111",  X"00010003"),
        (C_ALU_CTO, C_PACKED_BYTE,      B"00000011_00111101_11111111_00000111",  X"02010803"),
        (C_ALU_CTO, C_PACKED_NONE,      B"11111111_11111111_11111111_11111111",  X"00000020"),
        (C_ALU_CTO, C_PACKED_HALF_WORD, B"11111111_11111111_11111111_11111111",  X"00100010"),
        (C_ALU_CTO, C_PACKED_BYTE,      B"11111111_11111111_11111111_11111111",  X"08080808")
      );

  begin
    -- Test all the patterns in the pattern array.
    for i in C_PATTERNS'range loop
      -- Set the input.
      s_op <= C_PATTERNS(i).op;
      s_src <= C_PATTERNS(i).src;
      s_packed_mode <= C_PATTERNS(i).packed_mode;

      -- Wait for the results.
      wait for 1 ns;

      --  Check the output.
      assert s_result = C_PATTERNS(i).result
        report "Bad result (" & integer'image(i) & "):" & lf &
               "  src:      " & to_string(s_src) & lf &
               "  result:   " & to_string(s_result) & lf &
               "  expected: " & to_string(C_PATTERNS(i).result)
          severity error;
    end loop;
    assert false report "End of test" severity note;
    --  Wait forever; this will finish the simulation.
    wait;
  end process;
end behavioral;

